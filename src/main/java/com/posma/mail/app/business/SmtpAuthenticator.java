package com.posma.mail.app.business;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Created by Oswaldo Zarate on 8/5/2016.
 */



@Component
public class SmtpAuthenticator extends Authenticator {

    @Value("${email.user}")
    private String username;

    @Value("${email.password}")
    private String password;

    public SmtpAuthenticator() {
        super();
    }

    @Override
    public PasswordAuthentication getPasswordAuthentication() {

        if ((username != null) && (username.length() > 0) && (password != null)
                && (password.length() > 0)) {

            return new PasswordAuthentication(username, password);
        }

        return null;
    }
}