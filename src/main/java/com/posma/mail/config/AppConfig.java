package com.posma.mail.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Jorge Maguiña on 8/5/2016.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.posma.mail.app")
public class AppConfig {

}
