package com.posma.mail;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class for running Spring Boot framework.<br/>
 * <b>Class</b>: AtlasChannelSalaryAccountAffiliationApplication<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;.<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;.<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 * <u>Service Provider</u>: BCP <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>William</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Feb 13, 2018 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */


@SpringBootApplication(
        scanBasePackages = {"com.posma.mail"})
public class PosmaSendMailApplication{
  /**
   * Main method.
   */
  public static void main(String[] args) {
    SpringApplication.run(PosmaSendMailApplication.class, args);
  }
}
